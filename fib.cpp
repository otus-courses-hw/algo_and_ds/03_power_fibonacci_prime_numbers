#include "fib.hpp"
#include <cstddef>

unsigned int Fibonacci::recursive(unsigned int number)
{
    unsigned int f0 = 0;
    unsigned int f1 = 1;
    if (number == 0)
        return f0;
    if (number == 1)
        return f1;

    return recursive(number-1) + recursive(number-2);
}

unsigned int Fibonacci::iterative(unsigned int number)
{
    unsigned int f0 = 0;
    unsigned int f1 = 1;

    if (number == 0)
        return f0;
    if (number == 1)
        return f1;

    unsigned int result = 0;
    for (std::size_t i = 2; i <= number; ++i)
    {
        result = f0 + f1;
        f0 = f1;
        f1 = result;
    }
    return result;
}
