#include "prime.hpp"
#include <cstddef>

unsigned int Prime::brute_force(unsigned int number)
{
    std::size_t counter = 0;

    for (std::size_t i = number; i >= 1; --i)
    {
        if (i == 1)
        {
            ++counter;
            break;
        }

        for(std::size_t j = i-1; j >= 1; --j)
        {
            if (j == 1)
                ++counter;
            else if (i % j == 0)
                break;

        }
    }
    return counter;
}
