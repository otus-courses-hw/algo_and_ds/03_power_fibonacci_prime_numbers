all: tests

tests: tests.o power.o fib.o prime.o
	g++ -O0 -o $@ $^

%.o: %.cpp
	g++ -O0 -o $@ -c $<

clean:
	rm *.o tests

pow:
	./tests --durations yes [power]

fib:
	./tests --durations yes [fib]

prime:
	./tests --durations yes [prime]

.PHONY: clean test_power
