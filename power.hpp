#include <cstddef>

struct Power
{
    static double iterative(double, std::size_t);
    static double iterative_multipliation(double, std::size_t);
    static double iterative_binary(double, std::size_t);
    static double recursive(double, std::size_t);
    static double recursive_binary(double, std::size_t);
    static double recursive_improved(double, std::size_t);
};
