struct Fibonacci
{
    static unsigned int recursive(unsigned int);
    static unsigned int iterative(unsigned int);
};
