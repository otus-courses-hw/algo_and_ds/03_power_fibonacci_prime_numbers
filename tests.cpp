#define CATCH_CONFIG_MAIN

#include <catch.hpp>
#include "power.hpp"
#include "fib.hpp"
#include "prime.hpp"

TEST_CASE("POWER ITERATIVE O(n)", "[power]")
{
    CHECK(Power::iterative(1.00000001, 100000000) == Approx(2.71).margin(0.01));
}
TEST_CASE("POWER ITERATIVE O(n/2 + logn)", "[power]")
{
    CHECK(Power::iterative_multipliation(1.00000001, 100000000) == Approx(2.71).margin(0.01));
}
TEST_CASE("POWER ITERATIVE O(logn)", "[power]")
{
    CHECK(Power::iterative_binary(1.00000001, 100000000) == Approx(2.71).margin(0.01));
}
TEST_CASE("POWER RECURSIVE O(n)", "[power]")
{
    CHECK(Power::recursive(1.0001, 10000) == Approx(2.71).margin(0.01));
}
TEST_CASE("POWER RECURSIVE O(logn)", "[power]")
{
    CHECK(Power::recursive_binary(1.00000001, 100000000) == Approx(2.71).margin(0.01));
}

TEST_CASE("FIBONACCI RECURSIVE O(n^2)", "[fib]")
{
    CHECK(Fibonacci::recursive(0) == 0);
    CHECK(Fibonacci::recursive(1) == 1);
    CHECK(Fibonacci::recursive(2) == 1);
    CHECK(Fibonacci::recursive(3) == 2);
    CHECK(Fibonacci::recursive(4) == 3);
    CHECK(Fibonacci::recursive(5) == 5);
    CHECK(Fibonacci::recursive(17) == 1597);
}
TEST_CASE("FIBONACCI ITERATIVE O(n)", "[fib]")
{
    CHECK(Fibonacci::iterative(0) == 0);
    CHECK(Fibonacci::iterative(1) == 1);
    CHECK(Fibonacci::iterative(2) == 1);
    CHECK(Fibonacci::iterative(3) == 2);
    CHECK(Fibonacci::iterative(4) == 3);
    CHECK(Fibonacci::iterative(5) == 5);
    CHECK(Fibonacci::iterative(17) == 1597);
}

TEST_CASE("PRIME BRUTE FORCE O(n^2)", "[prime]")
{
    CHECK(Prime::brute_force(1) == 1);
    CHECK(Prime::brute_force(2) == 2);
    CHECK(Prime::brute_force(3) == 3);
    CHECK(Prime::brute_force(15) == 7);
}
