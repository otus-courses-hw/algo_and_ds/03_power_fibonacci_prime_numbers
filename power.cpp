#include "power.hpp"

double Power::iterative(double base, std::size_t exponent)
{
    auto result = static_cast<decltype(base)>(1);
    for (std::size_t i = 0; i < exponent; ++i)
        result *= base;
    return result;
}
double Power::iterative_multipliation(double base, std::size_t exponent)
{
    auto result = static_cast<decltype(base)>(1);
    for (std::size_t i = exponent; i > 1;)
    {
        if (i % 2 == 1)
        {
            result *= base;
        }
        base *= base;
        i /= 2;
    }
    return result * base;
}
double Power::iterative_binary(double base, std::size_t exponent)
{
    auto result = static_cast<decltype(base)>(1);
    for (std::size_t i = exponent; i > 0;)
    {
        if (i % 2 == 1)
        {
            result *= base;
            --i;
        }
        base *= base;
        i /= 2;
    }
    return result;
}

double Power::recursive(double base, std::size_t exponent)
{
    if (exponent == 0)
        return 1.;
    return base * recursive(base, exponent - 1);
}

double Power::recursive_binary(double base, std::size_t exponent)
{
    if (exponent == 0) return 1.;
    if (exponent == 1) return base;
    if (exponent % 2 == 0)
    {
        double x = recursive_binary(base, exponent / 2);
        return x * x;
    }
    else
        return base * recursive_binary(base, exponent - 1);
}
